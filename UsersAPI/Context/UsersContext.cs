﻿using Microsoft.EntityFrameworkCore;
using UsersAPI.Models;


namespace UsersAPI.Context
{
    public class UserContext : DbContext
    {
        public UserContext(DbContextOptions<UserContext> options)
            : base(options)
        {
        }

        public DbSet<Users> Users { get; set; }
    }
}
