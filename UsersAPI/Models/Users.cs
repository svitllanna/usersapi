namespace UsersAPI.Models
{
        public class Users
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
    }
}
