README
About Project
UsersAPI is a Web API that can manage users stored in the database

Version Number
Version 1.0

Run Application
Run application command:
GET /api/users Get all users  
GET /api/users/{id} Get a user by ID  
POST /api/users Add a new user  
PUT /api/users/{id} Update an existing user  
DELETE /api/users/{id} Delete a user
